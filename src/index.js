import { io } from 'socket.io-client'
import $ from 'jquery'

require('./styles/style.css')
require('./styles/sidebar.css')

window.requestAnimationFrame =
  window.requestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.msRequestAnimationFrame
const canevas = document.getElementById('test')
const context = canevas.getContext('2d')
const socket = io('http://localhost:3000', { autoConnect: false, transports: ['websocket'] })

// generic object
const direction = {
  ArrowRight: 96,
  ArrowLeft: 48,
  ArrowUp: 144,
  ArrowDown: 0
}
const sprite = {
  map: null
}
let user = {
  username: null,
  spr: null,
  src: null,
  socketid: null,
  direction: 96,
  spritepos: 0,
  x: 0,
  y: 0,
  room: ''
}
const message = {
  username: null,
  room: null,
  date: null,
  message: null
}

// Drawing function
sprite.map = new Image()
sprite.map.src = './texture/Map.png'
sprite.map.onload = drawMap

function drawMap () {
  context.drawImage(sprite.map, 0, 0, 800, 600)
}
function Draw (Users) {
  drawMap()
  Users.forEach(element => {
    element.spr = new Image()
    element.spr.src = element.src
    console.log(element)
    drawchar(element)
  })
}
function drawchar (element) {
  // drawImage(image, sx, sy, swidth, sheight, x, y, width, height)
  // context.drawImage(sprt, i * 48, sprtdir, 48, 48, x, y, 48, 48)
  context.font = '10px Arial'
  context.drawImage(element.spr, (element.spritepos % 3) * 48, element.direction, 48, 48, element.x, element.y, 48, 48)
  context.fillText(element.username, element.x + 10, element.y - 10)
}
function CreateUser (name, sprname) {
  if (name) {
    user = {
      username: null,
      spr: null,
      src: null,
      socketid: null,
      direction: 96,
      spritepos: 0,
      x: 0,
      y: 150,
      room: ''
    }
    user.username = name
    user.src = './texture/' + sprname + '.png'
    console.log(user)
    socket.connect()
  }
}

// client function
socket.on('connect', () => {
  console.log('connect')
  user.socketid = socket.id
  socket.emit('new user', user)
})

socket.on('chat message', function (data) {
  console.log(data.username + ' : ' + data.message)
  $('#message-container').append('<div class="message">' +
    '<p class="username">' + data.username + ' :</p>' +
    '<p class="content">' + data.message + '</p>' +
    '<p class="date">(' + data.date + ')' + '</p>' +
    '</div>')
})

socket.on('new user', (newUser) => {
  console.log('list : ', newUser)
  socket.emit('chat message',
    {
      username: newUser.username,
      message: 'Is connected'
    })
})

socket.on('update', (Users) => {
  Draw(Users)
})

socket.on('load history', (data) => {
  $('#message-container').empty()
  data.forEach(i => {
    $('#message-container').append('<div class="message">' +
    '<p class="username">' + i.username + ' :</p>' +
    '<p class="content">' + i.message + '</p>' +
    '<p class="date">(' + i.date + ')' + '</p>' +
    '</div>')
  })
})

socket.on('join room', (room) => {
  const joinRoom = confirm('Do you want to speak with this person ?')
  console.log(joinRoom)
  if (joinRoom) {
    user.room = room
  } else {
    user.room = ''
  }
  socket.emit('join room', {
    username: user.username,
    answer: joinRoom,
    room: room
  })
})

socket.on('leave room', () => {
  socket.emit('leave room', {
    room: user.room
  })
  $('#message-container').empty()
  user.room = ''
})
socket.on('alert', (data) => {
  alert(data)
})
// exposed function
window.disconnect = function () {
  socket.disconnect()
  $('#message-container').empty()
  const Form = document.getElementById('Form')
  const Validate = document.getElementById('Game')
  const sidenav = document.getElementById('sidenav')

  Validate.style.display = 'none'
  Form.style.display = 'block'
  sidenav.style.display = 'none'

  alert('you are now disconnected')
}
window.show = function () {
  const Form = document.getElementById('Form')
  const Validate = document.getElementById('Game')
  const fromusername = document.getElementById('fname')
  const sprite = document.getElementById('sprite-selector')
  const sidenav = document.getElementById('sidenav')
  $('#message-container').empty()
  if (fromusername.value !== '') {
    sidenav.style.display = 'flex'
    Validate.style.display = 'block'
    Form.style.display = 'none'
    drawMap()
    CreateUser(fromusername.value, sprite.value)
  }
}
window.sendMessage = function (msg) {
  message.message = $('#message-input').val()
  console.log($('#message-input').val())
  message.username = user.username
  const date = new Date()
  socket.emit('chat message',
    {
      username: user.username,
      message: message.message,
      date: date.getDay() + ':' + date.getMonth() + ':' + date.getFullYear(),
      room: user.room
    })
}

// main event
document.onkeydown = function (e) {
  if (direction[e.key] === undefined) {
    return
  }
  user.direction = direction[e.key]
  switch (direction[e.key]) {
    case 144: // up
      if (user.y > 0) {
        user.y -= 2
      }
      break
    case 0: // down
      if (user.y < 600 - 48) {
        user.y += 2
      }
      break
    case 48: // left
      if (user.x > 0) {
        user.x -= 2
      }
      break
    case 96: // ArrowRight
      if (user.x < 800 - 48) {
        user.x += 2
        break
      }
  }
  user.spritepos += 1
  socket.emit('update', user)
}
