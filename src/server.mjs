import express from 'express'
import { createServer } from 'http'
import { Server } from 'socket.io'
const app = express()
const server = createServer(app)
const io = new Server(server)

const Users = []
const history = []

// History management
function archivate (msg) {
  for (let i = 0; i < history.length; i++) {
    if (history[i].room === msg.room) {
      if (history[i].history.length === 5) {
        history[i].history.shift()
      }
      history[i].history.push(msg)
      return true
    }
  }
  history.push({
    room: msg.room,
    history: [msg]
  })
  return true
}
function loadhistory (room) {
  for (let i = 0; i < history.length; i++) {
    if (history[i].room === room) {
      return history[i].history
    }
  }
}

// server fonctionality
io.on('connection', (socket) => {
  socket.on('disconnect', () => {
    let user = null
    for (let i = 0; i < Users.length; i++) {
      if (Users[i].socketid === socket.id) {
        user = Users.splice(i, 1)
      }
    }
    const date = new Date()
    if (user) {
      io.emit('chat message',
        {
          username: user[0].username,
          date: date.getDay() + ':' + date.getMonth() + ':' + date.getFullYear(),
          message: 'Is disconnected'
        })
      console.log('disconnected : ' + Users)
    }
  })

  socket.on('chat message', (msg) => {
    console.log(msg.username + ' : ' + msg.message)
    if (msg.room === '') {
      // io.emit('chat message', msg)
    } else {
      io.in(msg.room).emit('chat message', msg)
      archivate(msg)
      console.log(history)
    }
  })
  socket.on('new user', (newUser) => {
    console.log('new : ', newUser)
    let auck = true
    Users.forEach(element => {
      if (element.socketid === newUser.socketid) {
        auck = false
      }
    })
    if (auck) { Users.push(newUser) }
    console.log('list : ', Users)
    socket.broadcast.emit('new user', newUser)
    const date = new Date()
    io.emit('chat message',
      {
        date: date.getDay() + ':' + date.getMonth() + ':' + date.getFullYear(),
        username: newUser.username,
        message: 'Is connected'
      })
  })
  socket.on('update', (newUser) => {
    Users.forEach(element => {
      if (element.socketid === newUser.socketid) {
        element.x = newUser.x
        element.y = newUser.y
        element.direction = newUser.direction
        element.spritepos = newUser.spritepos
      }
    })
    // console.log('list : ', Users)
    const user2 = checkproximity(newUser)
    if (user2 && user2.room === '' && newUser.room === '') {
      console.log('update : ', 'ok')
      console.log('room : ' + newUser.username + user2.username)
      socket.emit('join room', newUser.username + user2.username)
      socket.broadcast.to(user2.socketid).emit('join room', newUser.username + user2.username)
    }
    io.emit('update', Users)
  })

  socket.on('join room', (reply) => {
    if (reply.answer) {
      Users.forEach(element => {
        if (element.socketid === socket.id) {
          element.room = reply.room
        }
      })
      const date = new Date()
      socket.join(reply.room)
      io.in(reply.room).emit('chat message', {
        username: reply.username,
        date: date.getDay() + ':' + date.getMonth() + ':' + date.getFullYear(),
        message: 'join ' + reply.room
      })
      const h = loadhistory(reply.room)
      console.log(h)
      socket.emit('load history', h)
    } else {
      io.in(reply.room).emit('leave room', reply.room)
      io.in(reply.room).emit('alert', reply.username + ' didn\'t wanted to chat')
    }
  })
  socket.on('leave room', (reply) => {
    socket.leave(reply.room)
    Users.forEach(element => {
      if (element.socketid === socket.id) {
        element.room = ''
      }
    })
  })
})
// server listening
server.listen(3000, () => {
  console.log('listening on *:3000')
})
// check if a chat can be instantiate
function checkproximity (element) {
  for (let i = 0; i < Users.length; i++) {
    const dist = (element.x - Users[i].x) ** 2 + (element.y - Users[i].y) ** 2
    console.log(dist)
    if (dist !== 0) {
      if (dist < 2000) {
        console.log(element.username + ' is close to ' + Users[i].username)
        return Users[i]
      } else {
        console.log('far')
        console.log(element.room)
        console.log(Users[i].room)
        if (element.room === Users[i].room && element.room !== '') {
          console.log('emit leave')
          io.in(element.room).emit('leave room', element.room)
          io.in(element.room).emit('alert', element.username + ' left the chat')
        }
      }
    }
  }
}
