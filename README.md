# Chat ! by Ait Elmenceur Ilyès

## Available Scripts

In the project directory, you can run(twice if error):

### `npm run build`

Serve the javascript and CSS files, needed on the HTML page.<br />

### `npm run server`

Runs the server .<br />

## Usage
- Launch the server and build the project with the commands indicated, then open the HTML file
- first, enter a username and choose a character then move around with your arrows  
- If you want to speak with someone, walk toward him and accept the prompt, if this person also accepts it, you will both be put in a unique room where you will be able to exchange! 
- Up to 5 messages are stored by the server instance.

## Dependencies :
    * express
    * socket.io
    * socket.io-client
    * jQuery

bundles with webpack

## Contributor 
 - Ait Elmenceur Ilyès
